import pygame


pygame.init()


class MemoryModule:
    """
    represents a (potentially) infinite array of memory cells
    only one cell in the memory module is active at any given time

    mem_wrap is the value at which the current index will wrap around to zero
        the default value of mem_wrap is None, which means that the current index will never wrap around to zero

    cell_wrap is the value at which a given cell will wrap around to zero
        the default value of cell_wrap is None, which means that the value of a given cell will never wrap around to zero
    """

    def __init__(self, mem_wrap=None, cell_wrap=None):
        self.mem = {}
        self.index = 0

        self.enable_mem_wrap = mem_wrap is not None
        self.mem_wrap = mem_wrap

        self.enable_cell_wrap = cell_wrap is not None
        self.cell_wrap = cell_wrap

        # used for debug and status output
        self.mem_max_access = 0
        self.mem_min_access = 0

    def sp_up(self):
        """
        make the next cell the current cell
        """
        self.index += 1
        if self.enable_mem_wrap:
            self.index %= self.mem_wrap

    def sp_down(self):
        """
        make the previous cell the current cell
        """
        self.index -= 1
        if self.enable_mem_wrap:
            self.index %= self.mem_wrap

    def increment(self):
        """
        increments the value of the current cell by one
        """
        self.value += 1
        if self.enable_cell_wrap:
            self.value %= self.cell_wrap
        self.update_mem_access()

    def decrement(self):
        """
        decrements the value of the current cell by one
        """
        self.value -= 1
        if self.enable_cell_wrap:
            self.value %= self.cell_wrap
        self.update_mem_access()

    @property
    def value(self):
        """
        return the value of the current cell
        """
        return self.mem.get(self.index, 0)

    @value.setter
    def value(self, value):
        """
        set the value of the current cell
        """
        self.mem[self.index] = value

    @property
    def mem_array(self):
        """
        return a list representation of the memory module
        """
        mem = []
        for i in range(self.mem_min_access, self.mem_max_access + 1):
            mem.append(self.mem.get(i, 0))
        return mem

    def update_mem_access(self):
        """
        update the minimum and maximum cells of the memory module that have been accessed
        """
        self.mem_max_access = max(self.mem_max_access, self.index)
        self.mem_min_access = min(self.mem_min_access, self.index)


class BrainFuckProgram:
    """
    represents a program in brainfuck

    the program has a program counter which is the index of the current instruction
    """

    def __init__(self, prog):
        self.prog = self.preprocess_prog(prog)
        self.pc = 0

        self.bracket_match = self.get_bracket_match(prog)

    @staticmethod
    def preprocess_prog(prog):
        """
        strips non-instruction characters from the program
        """
        new_prog = ""
        for ch in prog:
            if ch in "+-<>.,[]":
                new_prog += ch
        return new_prog

    @classmethod
    def from_file(cls, file):
        """
        loads a bf program from a file
        """
        return cls(file.read())

    @staticmethod
    def get_bracket_match(prog):
        """
        constructs a dict with indices of opening brackets `[` mapped to the indices of the corresponding closing brackets `]`
        and vice versa

        this map is used for the loop_start and loop_end methods to jump to the correct position in the program
        """
        openers = []
        bracket_match = {}
        for index, ch in enumerate(prog):
            if ch == "]":
                try:
                    opener = openers.pop(-1)
                except IndexError:
                    raise ValueError(
                        f"invalid program: unbalanced bracket at character {index + 1}"
                    )
                bracket_match[opener] = index
                bracket_match[index] = opener

            elif ch == "[":
                openers.append(index)

        if len(openers) > 0:
            raise ValueError(
                f"invalid program: unbalanced bracket at character {openers[-1] + 1}"
            )

        return bracket_match

    @property
    def done(self):
        """
        returns whether or not the program is terminated
        """
        return self.pc >= len(self.prog)

    @property
    def current_inst(self):
        """
        returns the current instruction
        """
        return self.prog[self.pc]


class BrainFuckInterpreter:
    """
    implements a brainfuck interpreter
    """

    def __init__(self, prog, mem_wrap=None, cell_wrap=None):
        self.mem = MemoryModule(mem_wrap=mem_wrap, cell_wrap=cell_wrap)
        self.cell_wrap = cell_wrap
        self.mem_wrap = mem_wrap

        if not isinstance(prog, BrainFuckProgram):
            prog = BrainFuckProgram(prog)

        self.prog = prog

    def execute(self):
        """
        reset the interpreter and execute the given program (prog)
        """
        while not self.prog.done:
            self.step()

    def step(self):
        """
        step through a single instruction
        """
        self.exec_inst(self.prog.current_inst)

    def exec_inst(self, inst):
        """
        determines which method to call to execute the given instruction (inst)
        if the character at the current pc is not a valid bf instruction, it is ignored
        """
        if inst == "+":
            self.increment()
        elif inst == "-":
            self.decrement()
        elif inst == ",":
            self.input()
        elif inst == ".":
            self.output()
        elif inst == "<":
            self.sp_down()
        elif inst == ">":
            self.sp_up()
        elif inst == "[":
            self.loop_start()
        elif inst == "]":
            self.loop_end()
        else:
            self.prog.pc += 1

    def increment(self):
        """
        method called for the `+` bf instruction

        increments the value at the current memory cell
        """
        self.mem.increment()
        self.prog.pc += 1

    def decrement(self):
        """
        method called for the `-` bf instruction

        decrements the value at the current memory cell
        """
        self.mem.decrement()
        self.prog.pc += 1

    def input(self):
        """
        method called for the `,` bf instruction

        stores the value of the ascii character input in the current memory cell
        """
        c = input()
        if c == "":
            raise ValueError("invalid input")
        else:
            self.mem.value = ord(c[0])
        self.prog.pc += 1

    def output(self):
        """
        method called for the `.` bf instruction

        outputs the ascii character for the value at the current memory cell
        """
        print(chr(self.mem.value), end="")
        self.prog.pc += 1

    def sp_down(self):
        """
        method called for the `<` bf instruction

        set the previous memory cell as the current memory cell
        """
        self.mem.sp_down()
        self.prog.pc += 1

    def sp_up(self):
        """
        method called for the `>` bf instruction

        set the next memory cell as the current memory cell
        """
        self.mem.sp_up()
        self.prog.pc += 1

    def loop_start(self):
        """
        method called for the `[` bf instruction

        checks the value at the current mem cell and if it is zero, it jumps the program counter to the matching (closing) loop end instruction `]`
        """
        if self.mem.value == 0:
            self.prog.pc = self.prog.bracket_match[self.prog.pc]
        else:
            self.prog.pc += 1

    def loop_end(self):
        """
        method called for the `]` bf instruction

        checks the value at the current mem cell and if it is not zero, it jumps the program counter to the matching (opening) loop start instruction `[`
        """
        if self.mem.value != 0:
            self.prog.pc = self.prog.bracket_match[self.prog.pc]
        else:
            self.prog.pc += 1


class InteractiveBrainfuck:
    """
    represents a drawable, interactive brainfuck interpreter
    """

    mem_font = pygame.font.SysFont('Consolas', 14)

    def __init__(self, prog, cell_wrap=None, mem_wrap=None):
        self.interpreter = BrainFuckInterpreter(prog, cell_wrap=cell_wrap, mem_wrap=mem_wrap)

        self.display = pygame.display.set_mode((800, 500))
        pygame.display.set_caption("brainfuck interpreter")

        self.clock = pygame.time.Clock()
        self.done = False

        self.instruction_cooldown = 1
        self.current_instruction_cooldown = self.instruction_cooldown

    def start(self):
        self.done = False
        self.run()

    def run(self):
        while not self.done:
            self.events()
            self.update()
            self.render()

    def events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.done = True

    def update(self):
        dt = self.clock.tick(120) / 1000
        self.current_instruction_cooldown -= dt

        if self.current_instruction_cooldown <= 0:
            if not self.interpreter.prog.done:
                self.interpreter.step()
            self.current_instruction_cooldown %= self.instruction_cooldown

    def render(self):
        self.display.fill((0, 0, 0))
        pygame.display.flip()


if __name__ == "__main__":
    test_prog = "+[>>>->-[>->----<<<]>>]>.---.>+..+++.>>.<.>>---.<<<.+++.------.<-.>>+."
    test_prog = '+[>+]'
    bfi = InteractiveBrainfuck(test_prog, cell_wrap=256)
    bfi.start()
